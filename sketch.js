var N = 4;
var BW, BH; // Button width, button height
var FW, FH;
var DY;
var state = 1;
var opciones;
var nopciones;
var indices;

var colors;
var colorblue;
// COLORS http://www.colourlovers.com/palette/1614942/Candy_Playground

var botones;

var font;

function preload(){
	opciones = loadStrings("opciones.txt",stringsAreLoaded);	

	font = loadFont("Aleo-Regular.otf");

}

function stringsAreLoaded(){
	nopciones = opciones.length;
}

function setup() {
	createCanvas(displayWidth, displayHeight);
	createP("Lotería (BETA) | Desarrollado por José Vega-Cebrián (Sejo) | <a href='http://escenaconsejo.org'>Escenaconsejo</a> | Ago 2016 | Built with <a href='http://p5js.org'>p5.js</a> | <a href='https://github.com/Escenaconsejo/loteria'>Code</a>");

	FW = 1;
	FH = 0.55;
	DY = 0.15;
	BW = FW*displayWidth / N;
	BH = FH*displayHeight / N;

	background(255);

	botones = [];

	indices = [];

	colors = [color('#E89F9F'),color('#F0CB96'),color('#F3E787'),color('#AFD073')];
	colorblue = color('#84B7BB');


	textFont(font);
	textAlign(CENTER);
	textSize(12);
	strokeWeight(5);

	generate();

}

function draw() {
	background(255);
	textSize(27);
	fill(0);
	noStroke();
	textAlign(LEFT);
	text("Lotería",10,DY*displayHeight/2);

	textSize(12);

	text("Observa danza contemporánea. Presiona una casilla cuando veas lo que en ella se indica. Si llenas la planilla, grita LOTERÍA.",0.35*displayWidth,DY*displayHeight/4,displayWidth*0.65,displayHeight*DY);

	textSize(12);
	textAlign(CENTER);

	translate((1-FW)*displayWidth,DY*displayHeight);
	for(var i=0;i < botones.length; i++){
		botones[i].draw();
	}

	translate(0,FH*displayHeight);
	stroke(255);
	fill(colorblue);
	rect(displayWidth*FW/4,10,displayWidth*FW/2,displayHeight*(0.1));
	noStroke();
	fill(0);
	textAlign(CENTER);
	text("Genera otra planilla",displayWidth*FW/4,10+displayHeight*0.05,displayWidth*FW/2,displayHeight*0.05);
  
}

function mouseReleased(){
	for(var i=0;i < botones.length; i++){
		botones[i].pressed(mouseX,mouseY-0.1*displayHeight);
	}

	if(mouseY>=displayHeight*(FH+DY) && mouseY<=displayHeight*(FH+DY)+displayHeight*(0.1) && mouseX>=displayWidth*FW/4 && mouseX<=displayWidth*FW*3/4){
		generate();
	}
	return false;

}

function generate(){
	indices = [];
	botones = [];

	var sel;
	for(var i=0;i<N*N;i++){
		do{
			sel = int(floor(random(nopciones)));
		}while(inIndices(sel));
		indices.push(sel);
		println(sel);
	}

	var b;
	for(var i=0;i<indices.length;i++){
		b = new boton(opciones[indices[i]],i);
		botones.push(b);
	}

}

function inIndices(x){
	for(var i=0;i<indices.length;i++){
		if(indices[i]==x){
			return true;
		}
	}
	return false;

}


function boton(t,id){
	this.texto = t;
	this.x = BW*(id%N);
	this.y = BH*floor(id/N);
	this.selected = false;

	
	this.color = colors[floor((id+id/N)%colors.length)];

	this.draw = function (){
		stroke(255);
		if(this.selected){
			fill(colorblue);
		}
		else{
			fill(this.color);
		}
		rect(this.x, this.y, BW, BH);
		fill(0);
		noStroke();
		text(this.texto,this.x+BW*0.1,this.y+BH*0.1,BW*0.8,BH*0.8);

		if(this.selected){
			stroke(255,100);
			line(this.x,this.y,this.x+BW,this.y+BH);
			line(this.x,this.y+BH,this.x+BW,this.y);
		}
	}

	this.toggle = function(){
		this.selected = !this.selected;
	}

	this.pressed = function(x,y){
		if(x>=this.x && x<=this.x+BW && y>=this.y && y<=this.y+BH){
			this.toggle();
		}
		
	}

}
